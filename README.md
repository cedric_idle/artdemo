# ARTdemo

# Demo or ART Toolkit

1. Create a new Project, "Create an empty Project"
2. Name it, e.g. ART Demo
3. (+) `Add to Project` > `Notebook` > `From URL`
4. In Lower Right, Notebook URL, use `https://gitlab.com/whendrik/artdemo/-/raw/master/DEMO%20Art.ipynb`
5. Name it, e.g. "ART Demo with MNIST"

Execute the cells, discuss what happens in each cell!

## Questions:

- At what point does ART throw digital dirt in the training set?
- What is the weakness of CNN?
- Is there an alternative for CNN?